package de.Ryeera.NetAtmoX.Utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * General IO stream manipulation utilities.
 * <p>
 * This class provides static utility methods for input/output operations.
 * <ul>
 * <li>toXxx/read - these methods read data from a stream
 * <li>copy - these methods copy all the data from one stream to another
 * </ul>
 * <p>
 * All the methods in this class that read a stream are buffered internally. This means that there
 * is no cause to use a {@link BufferedInputStream} or {@link BufferedReader}. The default
 * buffer size of 4K has been shown to be efficient in tests.
 * <p>
 * Origin of code: Excalibur.
 */
public class IOUtils{
	
	/**
	 * Gets the contents of an {@link InputStream} as a <code>byte[]</code>.
	 * <p>
	 * This method buffers the input internally, so there is no need to use a
	 * {@link BufferedInputStream}.
	 * 
	 * @param input the {@link InputStream} to read from
	 * @return the requested byte array
	 * @throws IOException if an I/O error occurs
	 */
	public static byte[] toByteArray(InputStream input) throws IOException{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		copy(input, output);
		return output.toByteArray();
	}
	
	/**
	 * Copies bytes from an {@link InputStream} to an {@link OutputStream}.
	 * <p>
	 * This method buffers the input internally, so there is no need to use a
	 * {@link BufferedInputStream}.
	 * <p>
	 * Large streams (over 2GB) will return a bytes copied value of -1 after the copy
	 * has completed since the correct number of bytes cannot be returned as an int.
	 * 
	 * @param input the {@link InputStream} to read from
	 * @param output the {@link OutputStream} to write to
	 * @return the number of bytes copied, or -1 if &gt; Integer.MAX_VALUE
	 * @throws IOException if an I/O error occurs
	 * @since 1.0
	 */
	public static int copy(InputStream input, OutputStream output) throws IOException{
		long count = copyLarge(input, output, new byte[4096]);
		if(count > Integer.MAX_VALUE){ return -1; }
		return (int) count;
	}

	/**
	 * Copies bytes from a large (over 2GB) {@link InputStream} to an {@link OutputStream}.
	 * <p>
	 * This method uses the provided buffer, so there is no need to use a
	 * {@link BufferedInputStream}.
	 * 
	 * @param input the {@link InputStream} to read from
	 * @param output the {@link OutputStream} to write to
	 * @param buffer the buffer to use for the copy
	 * @return the number of bytes copied
	 * @throws IOException if an I/O error occurs
	 * @since 1.0
	 */
	public static long copyLarge(InputStream input, OutputStream output, byte[] buffer) throws IOException{
		long count = 0;
		int n = 0;
		while(-1 != (n = input.read(buffer))){
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}
}
