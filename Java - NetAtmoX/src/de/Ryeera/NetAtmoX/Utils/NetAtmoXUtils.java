package de.Ryeera.NetAtmoX.Utils;

import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

import de.Ryeera.NetAtmoX.Measurement;
import de.Ryeera.NetAtmoX.Constants.FeelLikeAlgorithm;
import de.Ryeera.NetAtmoX.Constants.MeasureType;
import de.Ryeera.NetAtmoX.Exceptions.NoMeasurementException;

/**
 * Class with utility-stuff
 * 
 * @author Neji3971
 * @since 1.0
 */
public class NetAtmoXUtils{

	public static HashMap<String, String> parseOAuthResponse(JSONObject response) throws JSONException{
		HashMap<String, String> parsedResponse = new HashMap<String, String>();
		parsedResponse.put("refresh_token", response.getString("refresh_token"));
		parsedResponse.put("access_token", response.getString("access_token"));
		parsedResponse.put("expires_at", new Long(System.currentTimeMillis() + Long.valueOf(response.getLong("expires_in")) * 1000).toString());
		return parsedResponse;
	}
	
	public static double calculateFeelsLikeTemperature(Measurement measure, FeelLikeAlgorithm algo) throws NoMeasurementException {
		return calculateFeelsLikeTemperature(measure.getData(MeasureType.TEMPERATURE), measure.getData(MeasureType.HUMIDITY), algo);
	}
	
	public static double calculateFeelsLikeTemperature(double T, double RH, FeelLikeAlgorithm algo) {
		if(algo == FeelLikeAlgorithm.HUMIDEX) {
			return calculateFeelsLikeTemperature(T, calculateDewPoint(T, RH));
		}else {
			return 0.0;
		}
	}
	
	public static double calculateFeelsLikeTemperature(double T, double DP) {
		return T + 0.5555*(6.11*Math.pow(Math.E, 5417.753*(1.0/273.16-1.0/(273.15+DP)))-10.0);
	}
	
	public static double calculateDewPoint(double T, double RH) {
		double b = 17.67;
		double c = 243.5;
		return (c*mue(T, RH))/(b-mue(T, RH));
	}
	
	private static double mue(double T, double RH) {
		double b = 17.67;
		double c = 243.5;
		return Math.log(RH/100.0)+(b*T)/(c+T);
	}
}