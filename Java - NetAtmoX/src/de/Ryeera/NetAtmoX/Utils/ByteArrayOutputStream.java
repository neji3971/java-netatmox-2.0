package de.Ryeera.NetAtmoX.Utils;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements an {@link OutputStream} in which the data is written into a byte array. The buffer
 * automatically grows as data is written to it.
 * <p>
 * The data can be retrieved using {@link #toByteArray()}.
 * <p>
 * Closing a {@link ByteArrayOutputStream} has no effect. The methods in this class can be called
 * after the stream has been closed without generating an {@link IOException}.
 * <p>
 * This is an alternative implementation of the {@link java.io.ByteArrayOutputStream} class. The
 * original implementation only allocates 32 bytes at the beginning. As this class is designed for
 * heavy duty it starts at 1024 bytes. In contrast to the original it doesn't reallocate the whole
 * memory block but allocates additional buffers. This way no buffers need to be garbage collected
 * and the contents don't have to be copied to the new buffer. This class is designed to behave
 * exactly like the original.
 */
public class ByteArrayOutputStream extends OutputStream{

	/** The list of buffers, which grows and never reduces. */
	private final List<byte[]>	buffers				= new ArrayList<byte[]>();
	/** The index of the current buffer. */
	private int					currentBufferIndex;
	/** The total count of bytes in all the filled buffers. */
	private int					filledBufferSum;
	/** The current buffer. */
	private byte[]				currentBuffer;
	/** The total count of bytes written. */
	private int					count;

	/**
	 * Creates a new {@link ByteArrayOutputStream}. The buffer capacity is initially 1024 bytes, though
	 * its size increases if necessary.
	 */
	public ByteArrayOutputStream(){
		this(1024);
	}

	/**
	 * Creates a new {@link ByteArrayOutputStream}, with a buffer capacity of the specified size, in
	 * bytes.
	 *
	 * @param size the initial size
	 * @throws IllegalArgumentException if size is negative
	 */
	public ByteArrayOutputStream(int size){
		if(size < 0){ throw new IllegalArgumentException("Negative initial size: " + size); }
		synchronized(this){
			needNewBuffer(size);
		}
	}

	/**
	 * Makes a new buffer available either by allocating a new one or re-cycling an existing one.
	 *
	 * @param newcount the size of the buffer if one is created
	 */
	private void needNewBuffer(int newcount){
		if(currentBufferIndex < buffers.size() - 1){
			filledBufferSum += currentBuffer.length;
			currentBufferIndex++;
			currentBuffer = buffers.get(currentBufferIndex);
		}else{
			int newBufferSize;
			if(currentBuffer == null){
				newBufferSize = newcount;
				filledBufferSum = 0;
			}else{
				newBufferSize = Math.max(currentBuffer.length << 1, newcount - filledBufferSum);
				filledBufferSum += currentBuffer.length;
			}
			currentBufferIndex++;
			currentBuffer = new byte[newBufferSize];
			buffers.add(currentBuffer);
		}
	}

	/**
	 * Write a byte to byte array.
	 * 
	 * @param b the byte to write
	 */
	@Override
	public synchronized void write(int b){
		int inBufferPos = count - filledBufferSum;
		if(inBufferPos == currentBuffer.length){
			needNewBuffer(count + 1);
			inBufferPos = 0;
		}
		currentBuffer[inBufferPos] = (byte) b;
		count++;
	}

	/**
	 * Gets the current contents of this byte stream as a byte array. The result is independent of
	 * this stream.
	 *
	 * @return the current contents of this output stream, as a byte array
	 * @see java.io.ByteArrayOutputStream#toByteArray()
	 */
	public synchronized byte[] toByteArray(){
		int remaining = count;
		if(remaining == 0){ return new byte[0]; }
		byte newbuf[] = new byte[remaining];
		int pos = 0;
		for(byte[] buf : buffers){
			int c = Math.min(buf.length, remaining);
			System.arraycopy(buf, 0, newbuf, pos, c);
			pos += c;
			remaining -= c;
			if(remaining == 0){
				break;
			}
		}
		return newbuf;
	}
}