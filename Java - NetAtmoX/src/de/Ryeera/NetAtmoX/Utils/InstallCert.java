package de.Ryeera.NetAtmoX.Utils;

import javax.net.ssl.*;
import javax.swing.JOptionPane;
import java.io.*;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Class used to add the server's certificate to the KeyStore with your trusted certificates.
 * @author Neji3971
 * @since 1.0
 */
public class InstallCert{

	/**
	 * Generates the neccessary SSL-Certificate which has to be put into the right location by the user manually
	 * @throws KeyStoreException 
	 * @throws IOException 
	 * @throws CertificateException 
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
	public static void install() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, KeyManagementException{
		System.out.println("Hi there! Your computer seems to have no SSL-certificate to use for api.netatmo.net...");
		System.out.println("Since I am a nice guy, I will just generate one.");
		File dir = new File(System.getProperty("java.home") + File.separator + "lib" + File.separator + "security");
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		InputStream in = new FileInputStream(new File(dir, "cacerts"));
		ks.load(in, new char[]{'c', 'h', 'a', 'n', 'g', 'e', 'i', 't'});
		in.close();
		SSLContext context = SSLContext.getInstance("TLS");
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(ks);
		X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
		TrustManager tm = new TrustManager(defaultTrustManager);
		context.init(null, new TrustManager[]{tm}, null);
		SSLSocket socket = (SSLSocket) context.getSocketFactory().createSocket("api.netatmo.net", 443);
		socket.setSoTimeout(10000);
		try{
			socket.startHandshake();
			socket.close();
		}catch(SSLException e){}
		X509Certificate[] chain = tm.chain;
		if(chain == null) return;
		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		for(X509Certificate cert : chain){
			sha1.update(cert.getEncoded());
			md5.update(cert.getEncoded());
		}
		ks.setCertificateEntry("api.netatmo.net-1", chain[0]);
		ks.store(new FileOutputStream("jssecacerts"), new char[]{'c', 'h', 'a', 'n', 'g', 'e', 'i', 't'});
		System.out.println("Creation of SSL-file finished!");
		System.out.println("Move the file and start the program again!");
		JOptionPane.showMessageDialog(null, "Move file \"jssecacerts\" to " + dir.getPath() + " and restart program!");
	}

	private static class TrustManager implements X509TrustManager{
		private final X509TrustManager	tm;
		private X509Certificate[]		chain;

		TrustManager(X509TrustManager tm){
			this.tm = tm;
		}

		public X509Certificate[] getAcceptedIssuers(){
			throw new UnsupportedOperationException();
		}

		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException{
			throw new UnsupportedOperationException();
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException{
			this.chain = chain;
			tm.checkServerTrusted(chain, authType);
		}
	}
}