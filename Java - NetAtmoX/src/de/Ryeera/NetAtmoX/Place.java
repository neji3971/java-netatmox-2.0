package de.Ryeera.NetAtmoX;

public class Place {

	private int altitude;
	private String city, country, timezone;
	private double latitude, longitude;
	
	public Place(double latitude, double longitude, int altitude, String city, String country, String timezone) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.city = city;
		this.country = country;
		this.timezone = timezone;
	}
	
	public Place(double latitude, double longitude, int altitude) {
		new Place(latitude, longitude, altitude, "", "", "");
	}

	public int getAltitude() {
		return altitude;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getTimezone() {
		return timezone;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	@Override
	public String toString() {
		return "Place [altitude=" + altitude + ", city=" + city + ", country=" + country + ", timezone=" + timezone
				+ ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
}