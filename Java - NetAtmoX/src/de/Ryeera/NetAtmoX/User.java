package de.Ryeera.NetAtmoX;

import java.util.ArrayList;
import java.util.List;

import de.Ryeera.NetAtmoX.Constants.FeelLikeAlgorithm;
import de.Ryeera.NetAtmoX.Constants.Units.PressureUnit;
import de.Ryeera.NetAtmoX.Constants.Units.Unit;
import de.Ryeera.NetAtmoX.Constants.Units.WindUnit;
import de.Ryeera.NetAtmoX.Modules.Station;

public class User{

	private String language, reg_locale, mail, country;
	private FeelLikeAlgorithm feelLikeAlgo;
	private PressureUnit pressureUnit;
	private Unit unit;
	private WindUnit windUnit;
	
	private List<Station> stations;

	

	public User(String language, String reg_locale, String mail, String country, FeelLikeAlgorithm feelLikeAlgo, PressureUnit pressureUnit, Unit unit, WindUnit windUnit) {
		this.language = language;
		this.reg_locale = reg_locale;
		this.mail = mail;
		this.country = country;
		this.feelLikeAlgo = feelLikeAlgo;
		this.pressureUnit = pressureUnit;
		this.unit = unit;
		this.windUnit = windUnit;
		this.stations = new ArrayList<>();
	}

	public List<Station> getStations(){
		return stations;
	}

	public void setStations(List<Station> stations){
		this.stations = stations;
	}

	public void addStation(Station station){
		stations.add(station);
	}

	public void clearStations(){
		stations.clear();
	}

	public String getLanguage() {
		return language;
	}

	public String getReg_locale() {
		return reg_locale;
	}

	public String getMail() {
		return mail;
	}

	public String getCountry() {
		return country;
	}

	public FeelLikeAlgorithm getFeelLikeAlgo() {
		return feelLikeAlgo;
	}

	public PressureUnit getPressureUnit() {
		return pressureUnit;
	}

	public Unit getUnit() {
		return unit;
	}

	public WindUnit getWindUnit() {
		return windUnit;
	}

	@Override
	public String toString() {
		return "User [language=" + language + ", reg_locale=" + reg_locale + ", mail=" + mail + ", country=" + country
				+ ", feelLikeAlgo=" + feelLikeAlgo + ", pressureUnit=" + pressureUnit + ", unit=" + unit + ", windUnit="
				+ windUnit + ", \nstations=" + stations + "\n]";
	}
}