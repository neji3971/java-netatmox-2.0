package de.Ryeera.NetAtmoX;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import de.Ryeera.NetAtmoX.Constants.FeelLikeAlgorithm;
import de.Ryeera.NetAtmoX.Constants.MeasureType;
import de.Ryeera.NetAtmoX.Constants.ModuleType;
import de.Ryeera.NetAtmoX.Constants.Units.PressureUnit;
import de.Ryeera.NetAtmoX.Constants.Units.Unit;
import de.Ryeera.NetAtmoX.Constants.Units.WindUnit;
import de.Ryeera.NetAtmoX.Exceptions.NetAtmoException;
import de.Ryeera.NetAtmoX.Modules.Module;
import de.Ryeera.NetAtmoX.Modules.Station;
import de.Ryeera.NetAtmoX.Utils.HTTPSUtils;
import de.Ryeera.NetAtmoX.Utils.NetAtmoXUtils;

/**
 * The HTTP-Client used to handle all requests to the API-Server
 * 
 * @author Neji3971
 * @version 1.2
 * @since 1.0
 */
public class NetAtmoXHttpsClient{
	final String	URL_BASE				= "https://api.netatmo.net";
	final String	URL_REQUEST_TOKEN		= URL_BASE + "/oauth2/token";
	final String	URL_GET_MEASUREMENT		= URL_BASE + "/api/getmeasure";
	final String	URL_GET_STAIONS			= URL_BASE + "/api/getstationsdata";
	private final String	clientId, clientSecret;
	private String			refreshToken, accessToken;
	private long			expiresAt;
	private JSONObject		lastResponse;
	
	/**
	 * The NetAtmo HTTPS-Client.
	 * This Object is used to connect to the API-Server and handle all requests.
	 * This is the first Object you have to create in order to use the API.
	 * Use {@link #login(String, String)} to login with your login-credentials.
	 * @param clientId
	 *            The client-ID you get when you create an app on <a
	 *            href="dev.netatmo.com/dev/createapp">NetAtmo Dev</a>
	 * @param clientSecret
	 *            The client-secret you get when you create an app on <a
	 *            href="dev.netatmo.com/dev/createapp">NetAtmo Dev</a>
	 * @since 1.0
	 */
	public NetAtmoXHttpsClient(String clientId, String clientSecret){
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}

	/**
	 * This is the first request you have to do before being able to use the API. It processes the
	 * response and stores all required tokens.
	 * 
	 * @param email The E-Mail-Address you used to create your NetAtmo Account
	 * @param password The corresponding password
	 * @throws IOException
	 * @since 1.0
	 */
	public void login(String email, String password) throws IOException{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("grant_type", "password");
		params.put("client_id", getClientId());
		params.put("client_secret", getClientSecret());
		params.put("username", email);
		params.put("password", password);
		HTTPSUtils http = new HTTPSUtils(new URL(URL_REQUEST_TOKEN), "POST", params);
		processOAuthResponse(http.getJsonFromHTTP());
	}

	/**
	 * Refreshes the access and the refresh-token using the refresh-token.
	 * @throws IOException
	 */
	private void refreshTokens() throws IOException{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("grant_type", "refresh_token");
		params.put("refresh_token", getRefreshToken());
		params.put("client_id", getClientId());
		params.put("client_secret", getClientSecret());
		HTTPSUtils http = new HTTPSUtils(new URL(URL_REQUEST_TOKEN), "POST", params);
		processOAuthResponse(http.getJsonFromHTTP());
	}

//	/**
//	 * Gets the defined {@link IndoorMeasurement}s from the server and sets it to the given {@link IndoorModule}.
//	 * 
//	 * @param module The {@link IndoorModule} you want to get measurements for
//	 * @param starttime
//	 *            The time in milliseconds from which on you want to acquire measurements<br>
//	 *            Set to -1 for last measurement only
//	 * @param endtime
//	 *            The time in milliseconds from which the last acquired measurement will be<br>
//	 *            Will be ignored if starttime is set to -1
//	 * @param scale
//	 *            The {@link Scale}<br>
//	 *            Will be ignored if starttime is set to -1
//	 * @param types
//	 *            The {@link MeasureType}s for which you want to acquire measurements. See <a
//	 *            href="https://dev.netatmo.com/doc/methods/getmeasure">NetAtmo Dev</a> for a list
//	 *            of applicable types.
//	 * @return A list of {@link IndoorMeasurement}s
//	 * @throws IOException
//	 * @throws JSONException
//	 * @since 1.0
//	 */
//	public List<IndoorMeasurement> getMeasurements(IndoorModule module, long starttime, long endtime, Scale scale, List<MeasureType> types) throws JSONException, IOException{
//		if(getExpiresAt() < System.currentTimeMillis()) refreshTokens();
//		String moduleid = "";
//		if(!module.getStation().getId().equals(module.getId())) moduleid = "&module_id=" + module.getId();
//		String typeurl = "";
//		if(types.size() > 0)typeurl = "&type=" + types.get(0).getTypeString();
//		if(types.size() > 1) for(int i = 1; i < types.size(); i++)
//			typeurl += "," + types.get(i).getTypeString();
//		URL url = new URL(URL_GET_MEASUREMENT + "?access_token=" + getAccessToken() + "&device_id=" + module.getStation().getId() + moduleid + (starttime == -1 ? "&scale=" + Scale.MAX.getScaleString() : "&scale=" + scale.getScaleString()) + typeurl
//				+ (starttime == -1 ? "" : "&date_begin=" + (starttime / 1000)) + (starttime == -1 ? "&date_end=last" : "&date_end=" + (endtime / 1000)) + "&real_time=true&optimize=false");
//		HTTPSUtils http = new HTTPSUtils(url);
//		JSONObject response = http.getJsonFromHTTP();
//		try{
//			JSONObject values = response.getJSONObject("body");
//			module.clearMeasurements();
//			Iterator<String> keys = values.keys();
//			while(keys.hasNext()){
//				IndoorMeasurement newMeasure = new IndoorMeasurement();
//				newMeasure.setMeasureTypes(types);
//				String key = keys.next();
//				JSONArray measures = values.getJSONArray(key);
//				int i = 0;
//				for(MeasureType type : types){
//					if(type == MeasureType.CO2) newMeasure.setCO2(measures.getInt(i));
//					else if(type == MeasureType.HUMIDITY) newMeasure.setHumidity(measures.getInt(i));
//					else if(type == MeasureType.MAX_HUMIDITY) newMeasure.setMaxHum(measures.getInt(i));
//					else if(type == MeasureType.MAX_NOISE) newMeasure.setMaxNoise(measures.getInt(i));
//					else if(type == MeasureType.MAX_PRESSURE) newMeasure.setMaxPress(measures.getDouble(i));
//					else if(type == MeasureType.MAX_TEMPERATURE) newMeasure.setMaxTemp(measures.getDouble(i));
//					else if(type == MeasureType.MIN_HUMIDITY) newMeasure.setMinHum(measures.getInt(i));
//					else if(type == MeasureType.MIN_NOISE) newMeasure.setMinNoise(measures.getInt(i));
//					else if(type == MeasureType.MIN_PRESSURE) newMeasure.setMinPress(measures.getDouble(i));
//					else if(type == MeasureType.MIN_TEMPERATURE) newMeasure.setMinTemp(measures.getDouble(i));
//					else if(type == MeasureType.NOISE) newMeasure.setNoise(measures.getInt(i));
//					else if(type == MeasureType.PRESSURE) newMeasure.setPressure(measures.getDouble(i));
//					else if(type == MeasureType.TEMPERATURE) newMeasure.setTemperature(measures.getDouble(i));
//					else if(type == MeasureType.DATE_MAX_CO2) newMeasure.setTimeMaxCO2(measures.getLong(i));
//					else if(type == MeasureType.DATE_MAX_HUMIDITY) newMeasure.setTimeMaxHum(measures.getLong(i));
//					else if(type == MeasureType.DATE_MAX_NOISE) newMeasure.setTimeMaxNoise(measures.getLong(i));
//					else if(type == MeasureType.DATE_MAX_PRESSURE) newMeasure.setTimeMaxPress(measures.getLong(i));
//					else if(type == MeasureType.DATE_MAX_TEMPERATURE) newMeasure.setTimeMaxTemp(measures.getLong(i));
//					else if(type == MeasureType.DATE_MIN_CO2) newMeasure.setTimeMinCO2(measures.getLong(i));
//					else if(type == MeasureType.DATE_MIN_HUMIDITY) newMeasure.setTimeMinHum(measures.getLong(i));
//					else if(type == MeasureType.DATE_MIN_NOISE) newMeasure.setTimeMinNoise(measures.getLong(i));
//					else if(type == MeasureType.MIN_PRESSURE) newMeasure.setTimeMinPress(measures.getLong(i));
//					else if(type == MeasureType.MIN_TEMPERATURE) newMeasure.setTimeMinTemp(measures.getLong(i));
//					i++;
//				}
//				newMeasure.setScale(scale);
//				newMeasure.setTime(Long.parseLong(key) * 1000);
//				module.addMeasurement(newMeasure);
//			}
//		}catch(JSONException e){
//			module.clearMeasurements();
//		}
//		return module.getCachedMeasurements();
//	}

	public User getUser() {
		while(true) {
			try {
				if(getExpiresAt() < System.currentTimeMillis()) refreshTokens();
				URL url = new URL(URL_GET_STAIONS + "?access_token=" + getAccessToken());
				HTTPSUtils http = new HTTPSUtils(url);
				lastResponse = http.getJsonFromHTTP();
				if(!lastResponse.has("status")) 							throw new NetAtmoException("Error while communicating with the API-server! Response: " + lastResponse.toString(2));
				if(!lastResponse.getString("status").equalsIgnoreCase("ok"))throw new NetAtmoException("Error while communicating with the API-server! Error-message: " + lastResponse.getString("status"));
				JSONObject body = lastResponse.getJSONObject("body");
				JSONArray devicesjson = body.getJSONArray("devices");
				JSONObject userjson = body.getJSONObject("user");
				JSONObject administrative = userjson.getJSONObject("administrative");
				User user = new User(administrative.getString("lang"), administrative.getString("reg_locale"), userjson.getString("mail"), administrative.getString("country"),
						FeelLikeAlgorithm.parseID(administrative.getInt("feel_like_algo")), PressureUnit.parseID(administrative.getInt("pressureunit")), Unit.parseID(administrative.getInt("unit")),
						WindUnit.parseID(administrative.getInt("windunit")));
				for(int i = 0; i < devicesjson.length(); i++) {
					JSONObject stationjson = devicesjson.getJSONObject(i);
					JSONObject placejson = stationjson.getJSONObject("place");
					Place place = new Place(placejson.getJSONArray("location").getDouble(0), placejson.getJSONArray("location").getDouble(1), placejson.getInt("altitude"), placejson.getString("city"),
							placejson.getString("country"), placejson.getString("timezone"));
					Station station = new Station(stationjson.getString("_id"), stationjson.getString("module_name"), stationjson.getLong("last_setup"), stationjson.getLong("last_status_store"),
							stationjson.getBoolean("reachable"), stationjson.getInt("firmware"), user, stationjson.getLong("date_setup"), stationjson.getLong("last_upgrade"),
							stationjson.getString("station_name"), stationjson.getInt("wifi_status"), stationjson.getBoolean("co2_calibrating"), place);
					if(stationjson.has("dashboard_data")) {
						JSONObject stationdashboardjson = stationjson.getJSONObject("dashboard_data");
						Measurement stationmeasurement = new Measurement();
						stationmeasurement.setTime(stationdashboardjson.getLong("time_utc"));
						for(MeasureType type : MeasureType.values()) {
							if(stationdashboardjson.has(type.getTypeString())) {
								stationmeasurement.setData(type, stationdashboardjson.getDouble(type.getTypeString()));
							}
						}
						stationmeasurement.setTemptrend(stationdashboardjson.optString("temp_trend"));
						stationmeasurement.setPressuretrend(stationdashboardjson.optString("pressure_trend"));
						station.setDashboard(stationmeasurement);
					}
					
					JSONArray modulesjson = stationjson.getJSONArray("modules");
					for(int j = 0; j < modulesjson.length(); j++) {
						JSONObject modulejson = modulesjson.getJSONObject(j);
						Module module = new Module(modulejson.getString("_id"), modulejson.getString("module_name"), modulejson.getLong("last_setup"), modulejson.getLong("last_seen"), 
								modulejson.getBoolean("reachable"), modulejson.getInt("firmware"), user, modulejson.getInt("rf_status"), modulejson.getInt("battery_vp"), modulejson.getInt("battery_percent"),
								modulejson.getString("type"));
						if(modulejson.has("dashboard_data")) {
							JSONObject moduledashboardjson = modulejson.getJSONObject("dashboard_data");
							Measurement measurement = new Measurement();
							measurement.setTime(moduledashboardjson.getLong("time_utc"));
							for(MeasureType type : MeasureType.values()) {
								if(moduledashboardjson.has(type.getTypeString())) {
									measurement.setData(type, moduledashboardjson.getDouble(type.getTypeString()));
								}
							}
							measurement.setTemptrend(moduledashboardjson.optString("temp_trend"));
							measurement.setPressuretrend(moduledashboardjson.optString("pressure_trend"));
							module.setDashboard(measurement);
						}
						if(module.getType() == ModuleType.OUTDOOR_MODULE) {
							station.setOutdoormodule(module);
						}else {
							station.addModule(module);
						}
					}
					user.addStation(station);
				}
				return user;
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error while getting user! Retrying...");
			}
		}
	}

	public String getClientId(){
		return clientId;
	}

	public String getClientSecret(){
		return clientSecret;
	}

	public void clearTokens(){
		refreshToken = "";
		accessToken = "";
		expiresAt = 0;
	}

	public String getRefreshToken(){
		return refreshToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public long getExpiresAt(){
		return expiresAt;
	}

	/**
	 * This creates a {@link HashMap} from the {@link JSONObject} acquired by {@link #login(String, String)} or {@link #refreshTokens()} and calls the {@link #storeTokens(String, String, long)} method
	 * @param response The {@link JSONObject} acquired by {@link #login(String, String)} or {@link #refreshTokens()}
	 */
	private void processOAuthResponse(JSONObject response){
		HashMap<String, String> parsedResponse = NetAtmoXUtils.parseOAuthResponse(response);
		storeTokens(parsedResponse.get("refresh_token"), parsedResponse.get("access_token"), Long.valueOf(parsedResponse.get("expires_at")));
	}

	/**
	 * Stores the tokens acquired by {@link #login(String, String)} or {@link #refreshTokens()} and parsed by {@link #processOAuthResponse(JSONObject)}.
	 * @param refreshToken The refresh-token to be stored. Can be used to refresh the access-token.
	 * @param accessToken The access-token to be stored
	 * @param expiresAt The time at which the access-token expires
	 */
	private void storeTokens(String refreshToken, String accessToken, long expiresAt){
		this.refreshToken = refreshToken;
		this.accessToken = accessToken;
		this.expiresAt = expiresAt;
	}
	
	public JSONObject getLastResponse() {
		return lastResponse;
	}
}