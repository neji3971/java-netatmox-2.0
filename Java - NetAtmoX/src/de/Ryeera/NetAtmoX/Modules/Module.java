package de.Ryeera.NetAtmoX.Modules;

import de.Ryeera.NetAtmoX.User;
import de.Ryeera.NetAtmoX.Constants.BatteryLevel;
import de.Ryeera.NetAtmoX.Constants.ModuleType;
import de.Ryeera.NetAtmoX.Constants.RadioStatus;

public class Module extends AbstractModule{

	private int 		rfStatus, batteryVP, batteryPercent;
	private ModuleType	type;
	
	public Module(String id, String name, long lastSetup, long lastSeen, boolean isReachable, int firmware, User user, int rfStatus, int batteryVP, int batteryPercent, ModuleType type) {
		super(id, name, lastSetup, lastSeen, isReachable, firmware, user);
		this.rfStatus = rfStatus;
		this.batteryVP = batteryVP;
		this.batteryPercent = batteryPercent;
		this.type = type;
	}
	
	public Module(String id, String name, long lastSetup, long lastSeen, boolean isReachable, int firmware, User user, int rfStatus, int batteryVP, int batteryPercent, String type) {
		super(id, name, lastSetup, lastSeen, isReachable, firmware, user);
		this.rfStatus = rfStatus;
		this.batteryVP = batteryVP;
		this.batteryPercent = batteryPercent;
		this.type = ModuleType.parseString(type);
	}

	public RadioStatus getRadioStatus() {
		return RadioStatus.parseSignal(rfStatus);
	}
	
	public BatteryLevel getBatteryLevel() {
		return BatteryLevel.parseLevel(type, batteryVP);
	}

	public int getRfStatus() {
		return rfStatus;
	}

	public void setRfStatus(int rfStatus) {
		this.rfStatus = rfStatus;
	}

	public int getBatteryVP() {
		return batteryVP;
	}

	public void setBatteryVP(int batteryVP) {
		this.batteryVP = batteryVP;
	}

	public int getBatteryPercent() {
		return batteryPercent;
	}

	public void setBatteryPercent(int batteryPercent) {
		this.batteryPercent = batteryPercent;
	}

	public ModuleType getType() {
		return type;
	}

	public void setType(ModuleType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "\nModule [rfStatus=" + rfStatus + ", batteryVP=" + batteryVP + ", batteryPercent=" + batteryPercent
				+ ", type=" + type + ", id=" + id + ", name=" + name + ", lastSetup=" + lastSetup + ", lastSeen="
				+ lastSeen + ", isReachable=" + isReachable + ", firmware=" + firmware + ", user=" + user.getMail()
				+ ", \ndashboard=" + dashboard + "]";
	}
}