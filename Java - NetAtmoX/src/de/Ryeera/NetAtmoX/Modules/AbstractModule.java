package de.Ryeera.NetAtmoX.Modules;

import de.Ryeera.NetAtmoX.Measurement;
import de.Ryeera.NetAtmoX.User;

/**
 * An abstract Module. It is inherited by other ModuleTypes.
 * @since 1.1
 */
abstract class AbstractModule{

	final String	id;
	String			name;
	long			lastSetup, lastSeen;
	boolean			isReachable;
	int				firmware;
	User			user;
	Measurement		dashboard;
	
	AbstractModule(String id, String name, long lastSetup, long lastSeen, boolean isReachable, int firmware, User user) {
		this.id = id;
		this.name = name;
		this.lastSetup = lastSetup;
		this.lastSeen = lastSeen;
		this.isReachable = isReachable;
		this.firmware = firmware;
		this.user = user;
		dashboard = new Measurement();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getLastSetup() {
		return lastSetup;
	}

	public void setLastSetup(long lastSetup) {
		this.lastSetup = lastSetup;
	}

	public long getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(long lastSeen) {
		this.lastSeen = lastSeen;
	}

	public boolean isReachable() {
		return isReachable;
	}

	public void setReachable(boolean isReachable) {
		this.isReachable = isReachable;
	}

	public int getFirmware() {
		return firmware;
	}

	public void setFirmware(int firmware) {
		this.firmware = firmware;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getId() {
		return id;
	}

	public Measurement getDashboard() {
		return dashboard;
	}

	public void setDashboard(Measurement dashboard) {
		this.dashboard = dashboard;
	}
	
	public long getDashboardTime() {
		return dashboard.getTime();
	}
}