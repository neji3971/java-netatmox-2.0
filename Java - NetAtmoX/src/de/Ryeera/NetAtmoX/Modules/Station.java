package de.Ryeera.NetAtmoX.Modules;

import java.util.ArrayList;
import java.util.List;

import de.Ryeera.NetAtmoX.Place;
import de.Ryeera.NetAtmoX.User;
import de.Ryeera.NetAtmoX.Constants.WifiStatus;

public class Station extends AbstractModule {

	private long dateSetup, lastUpgrade;
	private String stationName;
	private int wifiSignal;
	private boolean co2Calibrating;
	private Place place;
	private List<Module> modules;
	private Module outdoormodule;
	
	public Station(String id, String name, long lastSetup, long lastSeen, boolean isReachable, int firmware, User user,
			long dateSetup, long lastUpgrade, String stationName, int wifiSignal, boolean co2Calibrating, Place place) {
		super(id, name, lastSetup, lastSeen, isReachable, firmware, user);
		this.dateSetup = dateSetup;
		this.lastUpgrade = lastUpgrade;
		this.stationName = stationName;
		this.wifiSignal = wifiSignal;
		this.co2Calibrating = co2Calibrating;
		this.place = place;
		this.setOutdoormodule(null);
		this.modules = new ArrayList<>();
	}

	public WifiStatus getWifiStatus() {
		return WifiStatus.parseSignal(wifiSignal);
	}

	public long getDateSetup() {
		return dateSetup;
	}

	public void setDateSetup(long dateSetup) {
		this.dateSetup = dateSetup;
	}

	public long getLastUpgrade() {
		return lastUpgrade;
	}

	public void setLastUpgrade(long lastUpgrade) {
		this.lastUpgrade = lastUpgrade;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public int getWifiSignal() {
		return wifiSignal;
	}

	public void setWifiSignal(int wifiSignal) {
		this.wifiSignal = wifiSignal;
	}

	public boolean isCo2Calibrating() {
		return co2Calibrating;
	}

	public void setCo2Calibrating(boolean co2Calibrating) {
		this.co2Calibrating = co2Calibrating;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public List<Module> getModules() {
		return modules;
	}
	
	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
	
	public void addModule(Module module) {
		modules.add(module);
	}
	
	public void removeModule(Module module) {
		modules.remove(module);
	}
	
	public void clearModules() {
		modules.clear();
	}

	@Override
	public String toString() {
		return "\nStation [dateSetup=" + dateSetup + ", lastUpgrade=" + lastUpgrade + ", stationName=" + stationName
				+ ", wifiSignal=" + wifiSignal + ", co2Calibrating=" + co2Calibrating + ", id=" + id + ", name=" + name + ", lastSetup=" + lastSetup + ", lastSeen="
				+ lastSeen + ", isReachable=" + isReachable + ", firmware=" + firmware + ", user=" + user.getMail()
				+ ", place=" + place
				+ ", \ndashboard=" + dashboard + ", \nmodules=" + modules + "\n]";
	}

	public Module getOutdoormodule() {
		return outdoormodule;
	}

	public void setOutdoormodule(Module outdoormodule) {
		this.outdoormodule = outdoormodule;
	}
}