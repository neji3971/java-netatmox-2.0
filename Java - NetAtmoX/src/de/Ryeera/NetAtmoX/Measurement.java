package de.Ryeera.NetAtmoX;

import java.util.HashMap;

import de.Ryeera.NetAtmoX.Constants.MeasureType;
import de.Ryeera.NetAtmoX.Exceptions.NoMeasurementException;

public class Measurement{
	
	private long							time;
	private String							temptrend, pressuretrend;
	private HashMap<MeasureType, Double> 	data;

	public Measurement(){
		data = new HashMap<>();
		time = 0;
		temptrend = "";
		pressuretrend = "";
	}

	public void setData(MeasureType type, double value) {
		data.put(type, value);
	}
	
	public double getData(MeasureType type) throws NoMeasurementException{
		if(!hasData(type))throw new NoMeasurementException("There is no measurement of the type " + type.getTypeString());
		return data.get(type);
	}
	
	public boolean hasData(MeasureType type) {
		return data.containsKey(type);
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getTemptrend() {
		return temptrend;
	}

	public void setTemptrend(String temptrend) {
		this.temptrend = temptrend;
	}

	public String getPressuretrend() {
		return pressuretrend;
	}

	public void setPressuretrend(String pressuretrend) {
		this.pressuretrend = pressuretrend;
	}

	@Override
	public String toString() {
		return "Measurement [time=" + time + ", temptrend=" + temptrend + ", pressuretrend=" + pressuretrend + ", data=" + data + "]";
	}
}