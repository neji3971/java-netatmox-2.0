package de.Ryeera.NetAtmoX.Constants;

/**
 * All scales that are accepted by the API-Server
 * 
 * @author Neji3971
 * @since 1.0
 */
public enum Scale{
	MAX("max"), THIRTY_MINUTES("30min"), ONE_HOUR("1hour"), THREE_HOURS("3hours"), ONE_DAY("1day"), ONE_WEEK("1week"), ONE_MONTH("1month");

	private String	scale;

	Scale(String scale){
		this.scale = scale;
	}

	/**
	 * @return The String that is sent to the server
	 * @since 1.0
	 */
	public String getScaleString(){
		return scale;
	}
}