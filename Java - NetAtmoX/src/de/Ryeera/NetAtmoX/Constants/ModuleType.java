package de.Ryeera.NetAtmoX.Constants;

/**
 * All Types of modules
 * 
 * @author Neji3971
 * @since 1.0
 */
public enum ModuleType{
	INDOOR_MODULE("NAModule4"), OUTDOOR_MODULE("NAModule1"), RAIN_GAUGE("NAModule3"), STATION("NAMain"), WIND_GAUGE("NAModule2");

	private String	type;

	ModuleType(String type){
		this.type = type;
	}

	/**
	 * Returns a String that represents the module-type
	 * 
	 * @return how the module is called by the API-Server
	 * @since 1.0
	 */
	public String getTypeString(){
		return type;
	}
	
	public static ModuleType parseString(String type) {
		if(type.equalsIgnoreCase("NAModule1"))return OUTDOOR_MODULE;
		if(type.equalsIgnoreCase("NAModule4"))return INDOOR_MODULE;
		if(type.equalsIgnoreCase("NAModule3"))return RAIN_GAUGE;
		if(type.equalsIgnoreCase("NAModule2"))return WIND_GAUGE;
		if(type.equalsIgnoreCase("NAMain"))return STATION;
		return null;
	}
}