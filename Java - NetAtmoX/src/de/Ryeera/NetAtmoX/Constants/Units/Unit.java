package de.Ryeera.NetAtmoX.Constants.Units;

import de.Ryeera.NetAtmoX.User;
import de.Ryeera.NetAtmoX.Exceptions.IDOutOfRangeException;

/**
 * The Units in which measurements are displayed for the {@link User}
 * @author Neji3971
 * @since 1.2
 */
public enum Unit{

	METRIC(0, "metric system"),
	IMPERIAL(1, "imperial system");

	int id;
	String desc;
	
	Unit(int id, String desc){
		this.id = id;
		this.desc = desc;
	}
	
	/**
	 * @return The ID which is returned by the API-Server
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * @return The description of this Unit
	 */
	public String getDescriptor(){
		return desc;
	}
	
	/**
	 * Converts an ID to a Unit Enum
	 * @param id The ID to be converted, ranges from 0 to 1
	 * @return An enum representing the Unit
	 * @throws IDOutOfRangeException If id is more than 1 or less than 0
	 */
	public static Unit parseID(int id) throws IDOutOfRangeException{
		if(id == 0)return METRIC;
		else if(id == 1)return IMPERIAL;
		else throw new IDOutOfRangeException("There is no Unit with ID " + id);
	}
}