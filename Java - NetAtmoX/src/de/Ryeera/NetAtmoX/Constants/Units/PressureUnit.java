package de.Ryeera.NetAtmoX.Constants.Units;

import de.Ryeera.NetAtmoX.User;
import de.Ryeera.NetAtmoX.Exceptions.IDOutOfRangeException;

/**
 * The Units in which pressure are displayed for the {@link User}
 * @author Neji3971
 * @since 1.2
 */
public enum PressureUnit{

	MBAR(0, "mBar"),
	INHG(1, "inHg"),
	MMHG(2, "mmHg");

	int id;
	String desc;
	
	PressureUnit(int id, String desc){
		this.id = id;
		this.desc = desc;
	}
	
	/**
	 * @return The ID which is returned by the API-Server
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * @return The description of this PressureUnit
	 */
	public String getDescriptor(){
		return desc;
	}
	
	/**
	 * Converts an ID to a PressureUnit Enum
	 * @param id The ID to be converted, ranges from 0 to 2
	 * @return An enum representing the PressureUnit
	 * @throws IDOutOfRangeException If id is more than 2 or less than 0
	 */
	public static PressureUnit parseID(int id) throws IDOutOfRangeException{
		if(id == 0)return MBAR;
		else if(id == 1)return INHG;
		else if(id == 2)return MMHG;
		else throw new IDOutOfRangeException("There is no PressureUnit with ID " + id);
	}
}