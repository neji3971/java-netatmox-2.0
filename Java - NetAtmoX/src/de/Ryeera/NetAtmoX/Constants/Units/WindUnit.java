package de.Ryeera.NetAtmoX.Constants.Units;

import de.Ryeera.NetAtmoX.User;
import de.Ryeera.NetAtmoX.Exceptions.IDOutOfRangeException;

/**
 * The Units in which windspeeds are displayed for the {@link User}
 * @author Neji3971
 * @since 1.2
 */
public enum WindUnit{

	KPH(0, "kilometers per hour"),
	MPH(1, "miles per hour"),
	MS(2, "meters per second"),
	BEAUFORD(3, "Beauford"),
	KNOT(4, "Knots");
	
	int id;
	String desc;
	
	WindUnit(int id, String desc){
		this.id = id;
		this.desc = desc;
	}
	
	/**
	 * @return The ID which is returned by the API-Server
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * @return The description of this WindUnit
	 */
	public String getDescriptor(){
		return desc;
	}
	
	/**
	 * Converts an ID to a WindUnit Enum
	 * @param id The ID to be converted, ranges from 0 to 4
	 * @return An enum representing the WindUnit
	 * @throws IDOutOfRangeException If id is more than 4 or less than 0
	 */
	public static WindUnit parseID(int id) throws IDOutOfRangeException{
		if(id == 0)return KPH;
		else if(id == 1)return MPH;
		else if(id == 2)return MS;
		else if(id == 3)return BEAUFORD;
		else if(id == 4)return KNOT;
		else throw new IDOutOfRangeException("There is no WindUnit with ID " + id);
	}
	
}
