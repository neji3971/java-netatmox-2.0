package de.Ryeera.NetAtmoX.Constants;

import de.Ryeera.NetAtmoX.Modules.Module;

/**
 * The Battery-Level of a {@link WindGauge}
 * 
 * @author Ryeera
 * @since 2.0
 */
public enum BatteryLevel{
	MAX, FULL, HIGH, MEDIUM, LOW, VERYLOW;

	/**
	 * Parses an Integer that was returned by the API-Server to a battery-level based on the values defined for an IndoorModule.
	 * 
	 * @param level The level to parse
	 * @return A {@link BatteryLevel} that represents the level of charge
	 * @since 2.0
	 */
	public static BatteryLevel parseIndoorLevel(int level){
		if(level >= 6000) return MAX;
		else if(level >= 5640) return FULL;
		else if(level >= 5280) return HIGH;
		else if(level >= 4920) return MEDIUM;
		else if(level >= 4560) return LOW;
		else return VERYLOW;
	}
	
	/**
	 * Parses an Integer that was returned by the API-Server to a battery-level based on the values defined for an OutdoorModule.
	 * 
	 * @param level The level to parse
	 * @return An {@link BatteryLevel} that represents the level of charge
	 * @since 2.0
	 */
	public static BatteryLevel parseOutdoorLevel(int level){
		if(level >= 6000) return MAX;
		else if(level >= 5500) return FULL;
		else if(level >= 5000) return HIGH;
		else if(level >= 4500) return MEDIUM;
		else if(level >= 4000) return LOW;
		else return VERYLOW;
	}
	
	/**
	 * Parses an Integer that was returned by the API-Server to a battery-level based on the values defined for a RainGauge.
	 * 
	 * @param level The level to parse
	 * @return An {@link BatteryLevel} that represents the level of charge
	 * @since 2.0
	 */
	public static BatteryLevel parseRaingaugeLevel(int level){
		if(level >= 6000) return MAX;
		else if(level >= 5500) return FULL;
		else if(level >= 5000) return HIGH;
		else if(level >= 4500) return MEDIUM;
		else if(level >= 4000) return LOW;
		else return VERYLOW;
	}
	
	/**
	 * Parses an Integer that was returned by the API-Server to a battery-level based on the values defined for a WindGauge.
	 * 
	 * @param level The level to parse
	 * @return An {@link BatteryLevel} that represents the level of charge
	 * @since 2.0
	 */
	public static BatteryLevel parseWindgaugeLevel(int level){
		if(level >= 6000) return MAX;
		else if(level >= 5500) return FULL;
		else if(level >= 5000) return HIGH;
		else if(level >= 4500) return MEDIUM;
		else if(level >= 4000) return LOW;
		else return VERYLOW;
	}
	
	/**
	 * Parses an Integer that was returned by the API-Server to a battery-level based on the values defined for the {@link Module}
	 * 
	 * @param type The type of module to parse that level for
	 * @param level The level to parse
	 * @return An {@link BatteryLevel} that represents the level of charge
	 * @since 2.0
	 */
	public static BatteryLevel parseLevel(ModuleType type, int level){
		if(type == ModuleType.INDOOR_MODULE) {
			return parseIndoorLevel(level);
		}else if(type == ModuleType.OUTDOOR_MODULE) {
			return parseOutdoorLevel(level);
		}else if(type == ModuleType.RAIN_GAUGE) {
			return parseRaingaugeLevel(level);
		}else if(type == ModuleType.WIND_GAUGE) {
			return parseWindgaugeLevel(level);
		}else {
			return MAX;
		}
	}
}