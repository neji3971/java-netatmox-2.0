package de.Ryeera.NetAtmoX.Constants;

import de.Ryeera.NetAtmoX.Exceptions.IDOutOfRangeException;

/**
 * The algorithm used to compute feel-like temperatures
 * @author Neji3971
 * @since 1.2
 */
public enum FeelLikeAlgorithm{

	HUMIDEX(0, "humidex"),
	HEATINDEX(1, "heat-index");
	
	int id;
	String desc;
	
	private FeelLikeAlgorithm(int id, String desc){
		this.id = id;
		this.desc = desc;
	}
	
	/**
	 * @return The ID which is returned by the API-Server
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * @return The description of this Algorithm
	 */
	public String getDescriptor(){
		return desc;
	}
	
	/**
	 * Converts an ID to a Unit Enum
	 * @param id The ID to be converted, ranges from 0 to 1
	 * @return An enum representing the Algorithm
	 * @throws IDOutOfRangeException If id is more than 1 or less than 0
	 */
	public static FeelLikeAlgorithm parseID(int id) throws IDOutOfRangeException{
		if(id == 0)return HUMIDEX;
		else if(id == 1)return HEATINDEX;
		else throw new IDOutOfRangeException("There is no FeelLikeAlgorithm with ID " + id);
	}
}
