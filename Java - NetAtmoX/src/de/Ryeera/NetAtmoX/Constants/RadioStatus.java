package de.Ryeera.NetAtmoX.Constants;

/**
 * A status that indicates how good the signal is
 * 
 * @author Neji3971
 * @since 1.0
 */
public enum RadioStatus{
	LOW(90), MEDIUM(80), HIGH(70), FULL(60);

	private int	status;

	RadioStatus(int status){
		this.status = status;
	}

	/**
	 * @return The Intreger that represents the Radio-Signal
	 * @since 1.0
	 */
	public int getLevelInt(){
		return status;
	}

	/**
	 * Parses a signal returned by the API-server to a {@link RadioStatus}
	 * 
	 * @param signal
	 *            The signal to parse
	 * @return The {@link RadioStatus} that corresponds to the signal
	 * @since 1.0
	 */
	public static RadioStatus parseSignal(int signal){
		if(signal >= 90) return LOW;
		else if(signal >= 80) return MEDIUM;
		else if(signal >= 70) return HIGH;
		else return FULL;
	}
}
