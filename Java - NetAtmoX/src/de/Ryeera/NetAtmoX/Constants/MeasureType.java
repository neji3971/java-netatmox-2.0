package de.Ryeera.NetAtmoX.Constants;

/**
 * All Measure-Types the API-Server can accept
 * 
 * @author Neji3971
 * @since 1.0
 */
public enum MeasureType{
	//Scale: max, 30min, 1hour, 3hours, 1day, 1week, 1month
	TEMPERATURE			("Temperature"),
	CO2					("CO2"),
	HUMIDITY			("Humidity"),
	PRESSURE			("Pressure"),
	NOISE				("Noise"),
	RAIN				("Rain"),
	WIND_STRENGTH		("WindStrength"),
	WIND_ANGLE			("WindAngle"),
	GUST_STRENGTH		("GustStrength"),
	GUST_ANGLE			("GustAngle"),
	//Scale: 30min, 1hour, 3hours, 1day, 1week, 1month
	MIN_TEMPERATURE		("min_temp"),
	MAX_TEMPERATURE		("max_temp"),
	MIN_HUMIDITY		("min_hum"),
	MAX_HUMIDITY		("max_hum"),
	MIN_PRESSURE		("min_pressure"),
	MAX_PRESSURE		("max_pressure"),
	MIN_NOISE			("min_noise"),
	MAX_NOISE			("max_noise"),
	SUM_RAIN			("sum_rain"),
	DATE_MAX_GUST		("date_max_gust"),
	//Scale: 1day, 1week, 1month
	DATE_MIN_TEMPERATURE("date_min_temp"),
	DATE_MAX_TEMPERATURE("date_max_temp"),
	DATE_MIN_HUMIDITY	("date_min_hum"),
	DATE_MAX_HUMIDITY	("date_max_hum"),
	DATE_MIN_PRESSURE	("date_min_pressure"),
	DATE_MAX_PRESSURE	("date_max_pressure"),
	DATE_MIN_NOISE		("date_min_noise"),
	DATE_MAX_NOISE		("date_max_noise"),
	DATE_MIN_CO2		("date_min_co2"),
	DATE_MAX_CO2		("date_max_co2"),
	//Dashboard only
	ABSOLUTE_PRESSURE	("AbsolutePressure"),
	MAX_WIND_STR		("max_wind_str"),
	MAX_WIND_ANGLE		("max_wind_angle"),
	DATE_MAX_WIND_STR	("date_max_wind_str"),
	SUM_RAIN_24			("sum_rain_24");
	
	private String	type;

	MeasureType(String type){
		this.type = type;
	}

	/**
	 * @return the String that gets sent to the API-server
	 * @since 1.0
	 */
	public String getTypeString(){
		return type;
	}
}