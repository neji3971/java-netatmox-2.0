package de.Ryeera.NetAtmoX.Constants;

/**
 * A status that indicates how good the signal is
 * 
 * @author Neji3971
 * @since 1.0
 */
public enum WifiStatus{
	BAD, AVERAGE, GOOD;

	/**
	 * Parses a signal returned by the API-server to a {@link WifiStatus}
	 * 
	 * @param signal
	 *            The signal to parse
	 * @return The {@link WifiStatus} that corresponds to the signal
	 * @since 1.0
	 */
	public static WifiStatus parseSignal(int signal){
		if(signal <= 56)return GOOD;
		else if(signal <= 71)return AVERAGE;
		else return BAD;
	}
}
