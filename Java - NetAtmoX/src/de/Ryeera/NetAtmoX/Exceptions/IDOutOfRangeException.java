package de.Ryeera.NetAtmoX.Exceptions;

import de.Ryeera.NetAtmoX.Constants.MeasureType;
import de.Ryeera.NetAtmoX.Constants.Scale;

/**
 * An exception thrown by not using getMeasurement() correctly.
 * 
 * @author Neji3971
 * @since 1.2
 */
public class IDOutOfRangeException extends Exception{

	private static final long	serialVersionUID	= 7038815109963378635L;

	/**
	 * This Exception is thrown if you try to acquire a measurement with an incompatible {@link Scale} or {@link MeasureType} 
	 * @param message The message which is displayed
	 * @since 1.2
	 */
	public IDOutOfRangeException(String message){
		super(message);
	}
}