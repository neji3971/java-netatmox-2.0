package de.Ryeera.NetAtmoX.Exceptions;

public class NetAtmoException extends Exception{

	private static final long serialVersionUID = -233141015867434985L;

	public NetAtmoException(String message){
		super(message);
	}
}