package de.Ryeera.NetAtmoX.Exceptions;

import de.Ryeera.NetAtmoX.Measurement;

/**
 * An Exception thrown by trying to retrieve a type of measurement from a {@link Measurement} that is nonexistent.
 * 
 * @author Ryeera
 * @since 2.0
 */
public class NoMeasurementException extends Exception{

	private static final long serialVersionUID = 2326373786685061517L;

	/**
	 * This Exception is thrown if you try to retrieve a type of measurement from a {@link Measurement} that is nonexistent.
	 * @param message The message which is displayed
	 * @since 2.0
	 */
	public NoMeasurementException(String message){
		super(message);
	}
}