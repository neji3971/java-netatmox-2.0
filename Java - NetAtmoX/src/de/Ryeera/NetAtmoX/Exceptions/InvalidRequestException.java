package de.Ryeera.NetAtmoX.Exceptions;

/**
 * An exception thrown by trying to parse a Unit-Constant that does not exist.
 * 
 * @author Neji3971
 * @since 1.2
 */
public class InvalidRequestException extends Exception{

	private static final long	serialVersionUID	= 7038815109963378635L;

	/**
	 * This Exception is thrown if you try to parse an integer to any kind of Unit and there is no Unit with that ID 
	 * @param message The message which is displayed
	 * @since 1.2
	 */
	public InvalidRequestException(String message){
		super(message);
	}
}